# React Form
   This is a react form app built in Angular JS, in which there's an application form that feeds from Google API, validates the form and sends the response to the server.

 ## Quick Start
 - Install dependencies:
 #### Install dependencies:
 
```
 $ npm install
 ```
- To start the project:
 ####  To start the project:
 ```
 $ npm start
```

 - To initialize tests:
####  To initialize tests:
   
 ```
 $ npm test
 ```
   
 ## Author
  
 * **Vinicius Bernal** -  